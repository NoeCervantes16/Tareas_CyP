#include <stdio.h>

float fnPromedioEstatura(float estatura[]){
  float promedio=0.0;
  printf("calculando promedio...\n" );
  promedio=(estatura[0]+estatura[1]+estatura[2])/3.0;
  return promedio;
}

int main(int argc, char const *argv[]) {
  float estatura[3]={1.56,1.67,1.83};

  printf("Promedio de estaturas es %.2f metros\n",fnPromedioEstatura(estatura) );
  return 0;
}

